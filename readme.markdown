README
======

This is basically a fork of a cool little minimalistic typing program I found on the internet. It was posted as GPL, but the source was never published publicly, so I decompiled the .jar file and cleaned up the source code a bit, and decided to start adding some new functionality to it.

[Link](http://www.lifehackingmovie.com/2009/05/18/typewriter-minimal-text-editor-freeware/) to the original program.

### New Features (it's a lot, I know.)

* Removal of "No backspace / delete / select" functionality
* All Mac-related stuff is removed (feature, not bug!)
* Config file is now $HOME/.typewriter, instead of $HOME/Typewriter/typewriter.properties
* File open now defaults to "All files"
* New config options:
	* center.width: controls how wide the text area is in fullscreen
	* fontname: what font face to use
	* fontsize: what size to render the text
	* color_fg & color_bg: define colorscheme manually!

### TODO

* Toggle "Typewriter" mode (no delete / backspace / select)
* Whatever else I think of :D
