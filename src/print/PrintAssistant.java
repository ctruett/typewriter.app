package print;

import java.awt.Font;
import java.awt.print.PrinterException;
import javax.swing.JFrame;
import javax.swing.JTextArea;

public class PrintAssistant
  implements AssistantFace
{
  public void print(String txt, JFrame frame)
    throws PrinterException
  {
    JTextArea area = new JTextArea(txt);
    area.setFont(new Font("Serif", 0, 10));
    area.print();
  }
}