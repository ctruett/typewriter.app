package print;

import java.awt.print.PrinterException;
import javax.swing.JFrame;

public abstract interface AssistantFace
{
  public abstract void print(String paramString, JFrame paramJFrame)
    throws PrinterException;
}