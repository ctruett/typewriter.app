package typewriter;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Toolkit;
import java.awt.event.FocusListener;
import java.awt.event.KeyListener;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.awt.event.MouseWheelEvent;
import java.awt.event.MouseWheelListener;
import java.awt.print.PrinterException;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import javax.swing.Action;
import javax.swing.ActionMap;
import javax.swing.InputMap;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollBar;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.KeyStroke;
import javax.swing.event.CaretListener;
import javax.swing.text.DefaultEditorKit.InsertTabAction;
import print.AssistantFace;

public class TypeArea extends JPanel
{
  private JScrollPane jScrollPane;
  private JTextArea textArea;

  public TypeArea()
  {
    setLayout(new BorderLayout());

    this.textArea = new JTextArea();
    this.textArea.setText("");
    this.jScrollPane = new JScrollPane(this.textArea);

    add(this.jScrollPane, "Center");

    this.jScrollPane.setBorder(null);
    this.jScrollPane.setHorizontalScrollBarPolicy(31);
    this.jScrollPane.setVerticalScrollBarPolicy(21);
    this.jScrollPane.setPreferredSize(new Dimension(700, 500));

		Settings settings = new Settings();

		this.textArea.setColumns(10);
		this.textArea.setFont(settings.getFont());

    this.textArea.setLineWrap(true);
    this.textArea.setTabSize(4);
    this.textArea.setWrapStyleWord(true);
    this.textArea.setAutoscrolls(false);

    this.textArea.addMouseWheelListener(new MouseWheelListener() {
      public void mouseWheelMoved(MouseWheelEvent e) {
        int value = TypeArea.this.jScrollPane.getVerticalScrollBar().getValue();
        FontMetrics fm = TypeArea.this.textArea.getFontMetrics(TypeArea.this.textArea.getFont());
        int lineHeight = fm.getHeight();
        int newValue = value += e.getUnitsToScroll() * lineHeight;
        TypeArea.this.jScrollPane.getVerticalScrollBar().setValue(newValue);
      }
    });
  }

  public void requestFocus()
  {
    this.textArea.requestFocus();
  }

  public void addCaretListener(CaretListener caretListener)
  {
    this.textArea.addCaretListener(caretListener);
  }

  public void setColorScheme(ColorScheme scheme) {
    this.textArea.setForeground(scheme.getForeground());
    this.textArea.setBackground(scheme.getBackground());
    this.textArea.setCaretColor(scheme.getForeground());
  }

  public synchronized void putInputMap(KeyStroke k, String s, Action a) {
    this.textArea.getInputMap().put(k, s);
    this.textArea.getActionMap().put(s, a);
  }

  public synchronized void addKeyListener(KeyListener l)
  {
    this.textArea.addKeyListener(l);
  }

  public synchronized void addFocusListener(FocusListener l) {
    this.textArea.addFocusListener(l);
  }

  public void doPrint(JFrame frame) throws PrinterException {
    Toolkit kit = Toolkit.getDefaultToolkit();

    AssistantFace assistant = null;
    try {
      assistant = (AssistantFace)java.lang.Class.forName("print.PrintAssistant").getConstructors()[0].newInstance(new Object[0]);

      assistant.print(this.textArea.getText(), frame);
    } catch (InstantiationException e) {
      e.printStackTrace();
    } catch (IllegalAccessException e) {
      e.printStackTrace();
    } catch (InvocationTargetException e) {
      e.printStackTrace();
    } catch (ClassNotFoundException e) {
      e.printStackTrace();
    }
  }

  public String getText()
  {
    return this.textArea.getText();
  }

  public void setTheSize(int width, int height)
  {
    this.jScrollPane.setPreferredSize(new Dimension(width, height));
  }

  public void clear() {
    this.textArea.setText("");
  }

  public void appendLine(String line)
  {
    if (!this.textArea.getText().equals("")) {
      this.textArea.append("\r\n");
    }
    this.textArea.append(line);
  }

  /* private class CrippleArea extends JTextArea */
  /* { */
  /*   public CrippleArea() */
  /*   { */
  /*     ActionMap map = getActionMap(); */
  /*     map = map.getParent().getParent().getParent(); */
  /*     map.clear(); */
  /*     map.put("insert-tab", new DefaultEditorKit.InsertTabAction()); */

  /*     MouseListener[] ml = getMouseListeners(); */
  /*     for (MouseListener mouseListener : ml) { */
  /*       removeMouseListener(mouseListener); */
  /*     } */

  /*     MouseMotionListener[] mml = getMouseMotionListeners(); */
  /*     for (MouseMotionListener mouseMotionListener : mml) */
  /*       removeMouseMotionListener(mouseMotionListener); */
  /*   } */
  /* } */
}
