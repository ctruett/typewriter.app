package typewriter.dialog;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Container;
import java.awt.Window;
import java.awt.event.ActionEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.ActionMap;
import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.InputMap;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.KeyStroke;
import typewriter.ColorScheme;

public abstract class WriterDialog extends JDialog
{
  private JFrame frame;
  protected Box mainPanel = Box.createVerticalBox();
  protected Action closeAction;
  private JPanel wrapPanel;
  private Window window;
  protected int result = 2;

  private int parWidth = 0;
  private int parHeight = 0;

  public WriterDialog(JFrame f, Window w, int wid, int hei) {
    this.frame = f;
    this.window = w;

    this.parWidth = wid;
    this.parHeight = hei;

    setModal(true);
    setResizable(false);
    setUndecorated(true);

    getContentPane().setLayout(new BorderLayout());

    this.wrapPanel = new JPanel();
    this.mainPanel.setBorder(BorderFactory.createMatteBorder(30, 30, 20, 30, getBackground()));
    this.wrapPanel.setBorder(BorderFactory.createMatteBorder(2, 2, 2, 2, Color.DARK_GRAY));
    this.wrapPanel.add(this.mainPanel);

    add(this.wrapPanel, "Center");

    this.closeAction = new AbstractAction() {
      public void actionPerformed(ActionEvent e) {
        WriterDialog.this.setVisible(false);
      }
    };
    this.mainPanel.getInputMap().put(KeyStroke.getKeyStroke(27, 0), "close-frame");
    this.mainPanel.getActionMap().put("close-frame", this.closeAction);

    addMouseListener(new MouseAdapter() {
      public void mouseClicked(MouseEvent e) {
        if (e.getClickCount() > 1)
          WriterDialog.this.setVisible(false);
      }
    });
  }

  protected void setColorScheme(ColorScheme scheme)
  {
    setBackground(scheme.getBackground());
    setForeground(scheme.getForeground());
    this.mainPanel.setForeground(scheme.getForeground());

    this.mainPanel.setBorder(BorderFactory.createMatteBorder(30, 30, 20, 30, getBackground()));
    this.wrapPanel.setBorder(BorderFactory.createMatteBorder(2, 2, 2, 2, Color.DARK_GRAY));
  }

  public void showIt(ColorScheme scheme, boolean inFrame)
  {
    this.result = 2;
    pack();

    if (inFrame) {
      setLocationRelativeTo(this.frame);
      setLocation(this.frame.getX() + this.frame.getWidth() / 2 - getWidth() / 2, this.frame.getY() + this.frame.getHeight() / 2 - getHeight() / 2);
    }
    else {
      setLocationRelativeTo(this.window);
      setLocation(this.parWidth / 2 - getWidth() / 2, this.parHeight / 2 - getHeight() / 2);
    }

    setVisible(true);
  }
}