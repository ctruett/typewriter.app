package typewriter.dialog;

import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.Window;
import java.awt.event.ActionEvent;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.ActionMap;
import javax.swing.Box;
import javax.swing.InputMap;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.KeyStroke;
import typewriter.ColorScheme;

public class YesNoDialog extends WriterDialog
{
  private JLabel label = new JLabel();
  private JPanel buttonsPanel = new JPanel();
  private KeyListener anyKeyExit;

  public YesNoDialog(JFrame f, Window w, int wid, int hei)
  {
    super(f, w, wid, hei);

    Action yesAction = new AbstractAction("Yes")
    {
      public void actionPerformed(ActionEvent e) {
        YesNoDialog.this.result = 0;
        YesNoDialog.this.setVisible(false);
      }
    };
    Action noAction = new AbstractAction("No")
    {
      public void actionPerformed(ActionEvent e) {
        YesNoDialog.this.result = 1;
        YesNoDialog.this.setVisible(false);
      }
    };
    this.label.setFont(new Font("Courier New", 0, 14));

    this.buttonsPanel = new JPanel(new FlowLayout(1));
    JButton yesButton = new JButton(yesAction);
    JButton noButton = new JButton(noAction);

    this.buttonsPanel.add(yesButton);
    this.buttonsPanel.add(noButton);

    this.label.setAlignmentX(0.5F);
    this.buttonsPanel.setAlignmentX(0.5F);

    this.mainPanel.add(this.label);
    this.mainPanel.add(new JLabel(" "));
    this.mainPanel.add(this.buttonsPanel);

    yesButton.getInputMap().put(KeyStroke.getKeyStroke(27, 0), "close-frame");
    yesButton.getActionMap().put("close-frame", this.closeAction);
    noButton.getInputMap().put(KeyStroke.getKeyStroke(27, 0), "close-frame");
    noButton.getActionMap().put("close-frame", this.closeAction);

    this.mainPanel.getInputMap().put(KeyStroke.getKeyStroke(89, 0), "yes");
    this.mainPanel.getActionMap().put("yes", yesAction);
    yesButton.getInputMap().put(KeyStroke.getKeyStroke(89, 0), "yes");
    yesButton.getActionMap().put("yes", yesAction);
    noButton.getInputMap().put(KeyStroke.getKeyStroke(89, 0), "yes");
    noButton.getActionMap().put("yes", yesAction);

    this.mainPanel.getInputMap().put(KeyStroke.getKeyStroke(78, 0), "no");
    this.mainPanel.getActionMap().put("no", noAction);
    yesButton.getInputMap().put(KeyStroke.getKeyStroke(78, 0), "no");
    yesButton.getActionMap().put("no", noAction);
    noButton.getInputMap().put(KeyStroke.getKeyStroke(78, 0), "no");
    noButton.getActionMap().put("no", noAction);

    this.anyKeyExit = new KeyAdapter() {
      public void keyPressed(KeyEvent e) {
        YesNoDialog.this.setVisible(false);
      }
    };
  }

  public void showIt(String caption, ColorScheme scheme, boolean showButtons, boolean inFrame) {
    this.label.setText(caption);
    this.buttonsPanel.setVisible(showButtons);
    if (showButtons)
      this.mainPanel.removeKeyListener(this.anyKeyExit);
    else {
      this.mainPanel.addKeyListener(this.anyKeyExit);
    }
    showIt(scheme, inFrame);
  }

  public int getResult() {
    return this.result;
  }
}