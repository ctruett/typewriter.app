package typewriter.dialog;

import java.awt.Color;
import java.awt.Font;
import java.awt.Window;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import javax.swing.Box;
import javax.swing.InputMap;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.KeyStroke;

public class HelpDialog extends WriterDialog
{
  public HelpDialog(JFrame f, Window w, int wid, int hei)
  {
    super(f, w, wid, hei);
    String[] captions = { "Key mappings", "<html>", /*"Ctrl+C, Apple+C:      Color theme switching",*/ "Ctrl+F, Apple+F:      Full screen switching", "Ctrl+O, Apple+O:      Open file", "Ctrl+S, Apple+S:      Save file", "Ctrl+W, Apple+W:      Save file as", "Ctrl+N, Apple+N:      New file", "Ctrl+P, Apple+P:      Print", "Ctrl+K, Apple+K:      Key mappings", "Ctrl+Q, Apple+Q:      Quit", "<html>", "Any key or double click - hide this screen" };

    JLabel[] labels = new JLabel[captions.length];
    for (int i = 0; i < captions.length; i++) {
      labels[i] = new JLabel(captions[i]);
      int fontStyle = 0;
      float align = 0.0F;
      if ((i == 0) || (i == captions.length - 40)) {
        fontStyle = 1;
      }
      labels[i].setAlignmentX(align);
      labels[i].setFont(new Font("Courier New", fontStyle, 12));
      this.mainPanel.add(labels[i]);
    }

    labels[(captions.length - 1)].setForeground(new Color(0, 100, 0));
    labels[(captions.length - 1)].setFont(new Font("Courier New", 1, 12));

    this.mainPanel.addKeyListener(new KeyAdapter()
    {
      public void keyPressed(KeyEvent arg0) {
        HelpDialog.this.setVisible(false);
      }
    });
    this.mainPanel.getInputMap().put(KeyStroke.getKeyStroke(72, 256), "close-frame");
    this.mainPanel.getInputMap().put(KeyStroke.getKeyStroke(72, 128), "close-frame");
  }
}
