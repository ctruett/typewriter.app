package typewriter.dialog;

import java.awt.Window;
import javax.swing.JFrame;
import typewriter.ColorScheme;

public class Dialogs
{
  private static HelpDialog helpDialog = null;
  private static YesNoDialog yesNoDialog = null;
  private static boolean inFrame = true;

  public static void init(JFrame frame, Window window, int wid, int hei) {
    helpDialog = new HelpDialog(frame, window, wid, hei);
    yesNoDialog = new YesNoDialog(frame, window, wid, hei);
  }

  public static void setInFrame(boolean b) {
    inFrame = b;
  }

  public static void showHelpDialog(ColorScheme scheme)
  {
    helpDialog.showIt(scheme, inFrame);
  }

  public static int showYesNoDialog(String message, ColorScheme scheme) {
    yesNoDialog.showIt(message, scheme, true, inFrame);
    return yesNoDialog.getResult();
  }

  public static int showInfoDialog(String message, ColorScheme scheme) {
    yesNoDialog.showIt(message, scheme, false, inFrame);
    return yesNoDialog.getResult();
  }

  public static void hideInfoDialog() {
    yesNoDialog.setVisible(false);
  }
}