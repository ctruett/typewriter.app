package typewriter;

import java.awt.Component;
import javax.swing.SpringLayout;

public class FrameLayout extends SpringLayout
{
  FrameLayout(Component parent, Component content, int westMargin, int NorthMargin, int eastMargin, int SouthMargin)
  {
    putConstraint("North", content, NorthMargin, "North", parent);
    putConstraint("South", parent, SouthMargin, "South", content);
    putConstraint("East", parent, eastMargin, "East", content);
    putConstraint("West", content, westMargin, "West", parent);
  }
}