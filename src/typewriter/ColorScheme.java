package typewriter;

import java.awt.Color;

public class ColorScheme
{
  private Color foreground;
  private Color background;

  public ColorScheme(Color foreground, Color background)
  {
    this.foreground = foreground;
    this.background = background;
  }

  public Color getForeground() {
    return this.foreground;
  }

  public Color getBackground() {
    return this.background;
  }

  public boolean equals(Object obj) {
    if (!obj.getClass().equals(ColorScheme.class)) return false;
    ColorScheme oScheme = (ColorScheme)obj;
    return (this.foreground.equals(oScheme.foreground)) && (this.background.equals(oScheme.background));
  }
}