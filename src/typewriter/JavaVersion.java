package typewriter;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class JavaVersion
  implements Comparable
{
  private String strValue;
  private String[] version;

  public JavaVersion(String str)
  {
    this.version = new String[5];

    String digit = "\\d";
    String dot = "\\.";
    Pattern pattern = Pattern.compile("(" + digit + ")" + dot + "(" + digit + ")(" + dot + "(" + digit + ")(_(" + digit + digit + ")(.*))?)?");

    Matcher matcher = pattern.matcher(str);
    if ((str == null) || (!matcher.matches())) {
      this.strValue = "0.0.0_00";
      matcher = pattern.matcher(this.strValue);
      if (!matcher.matches())
        throw new AssertionError("Matcher must match zero version");
    }
    else {
      this.strValue = str;
    }

    int groups = matcher.groupCount();

    this.version[0] = matcher.group(1);
    this.version[1] = matcher.group(2);
    this.version[2] = matcher.group(4);
    this.version[3] = matcher.group(6);
    this.version[4] = matcher.group(7);

    for (int i = 0; i < 5; i++)
      if (this.version[i] == null)
        this.version[i] = "0";
  }

  public String toString()
  {
    return "[Java version: " + this.strValue + "]";
  }

  public String getMajor() {
    return this.version[0] + "." + this.version[1];
  }

  public static JavaVersion getCurrent() {
    return new JavaVersion(System.getProperty("java.version"));
  }

  public int compareTo(Object o) {
    if (!o.getClass().equals(JavaVersion.class)) {
      throw new IllegalArgumentException("JavaVersion can be compared only with another instance of JavaVersion");
    }

    JavaVersion other = (JavaVersion)o;

    for (int i = 0; i < 5; i++) {
      int res = this.version[i].compareTo(other.version[i]);
      if (res != 0) {
        return res;
      }
    }
    return 0;
  }
}