package typewriter;

import java.awt.Color;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Rectangle;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintStream;
import java.util.Properties;

public class Settings
{
  /* private static final ColorScheme blackwhite = new ColorScheme(Color.BLACK, Color.WHITE); */
  /* private static final ColorScheme greenblack = new ColorScheme(Color.GREEN, Color.BLACK); */

  public final ColorScheme standard = new ColorScheme(Color.GREEN, Color.BLACK);

  private Properties properties = new Properties();
  /* private static String filename = "typewriter.properties"; */
  private static String filename = ".typewriter";
  private static final String KEY_SHOWKEYS = "showkeys";
  private static final String KEY_COLORSCHEME = "colorscheme";
  private static final String KEY_WIN_X = "window.x";
  private static final String KEY_WIN_Y = "window.y";
  private static final String KEY_WIN_W = "window.width";
  private static final String KEY_FULLSCREEEN = "fullscreen";
  private static final String KEY_WIN_H = "window.height";
  /* private static final String SCHEME_GB = "greenblack"; */
  /* private static final String SCHEME_BW = "blackwhite"; */

  public ColorScheme getColorScheme()
  {
    /* String scheme = get("colorscheme", "greenblack"); */
    /* return scheme.equals("greenblack") ? greenblack : blackwhite; */
    String fg = "#" + get("color_fg", "00FF00");
    String bg = "#" + get("color_bg", "000000");
    ColorScheme theme = new ColorScheme(Color.decode(fg), Color.decode(bg));
    return theme;
  }

  public void setColorScheme(ColorScheme colorScheme) {
    set("colorscheme", "standard");
  }

  public Rectangle getPosition() {
    int x = getAsInt("window.x", 0);
    int y = getAsInt("window.y", 0);
    int w = getAsInt("window.width", 700);
    int h = getAsInt("window.height", 400);
    return new Rectangle(x, y, w, h);
  }

  public void setPosition(Rectangle r) {
    set("window.x", String.valueOf(r.x));
    set("window.y", String.valueOf(r.y));
    set("window.width", String.valueOf(r.width));
    set("window.height", String.valueOf(r.height));
  }

  public boolean getShowKeys()
  {
    return getAsBoolean("showkeys", true);
  }

  public void setShowKeys(boolean b) {
    set("showkeys", String.valueOf(b));
  }

  public boolean getFullScreen() {
    return getAsBoolean("fullscreen", false);
  }

  public void setFullScreen(boolean fs)
  {
    set("fullscreen", String.valueOf(fs));
  }

  public Font getFont() {
    String fontName = get("fontname", "Courier New");
    int fontSize= Integer.parseInt(get("fontsize", "14"));
    Font font = new Font(fontName, 0, fontSize);
    return font;
  }

  public int getCenterWidth() {
    int centerWidth = Integer.parseInt(get("center.width", "420"));
    return centerWidth;
  }

  public Settings()
  {
    String confLocation = System.getenv("HOME");

    String FNAME = ".typewriter";
    filename = ".typewriter";

    if ((confLocation != null) && (new File(confLocation).canWrite())) {
      filename = confLocation + File.separator + ".typewriter";
    } else {
      String home = System.getProperty("user.home");
      if (home != null) {
        File dir = new File(home);
        if (dir.canWrite()) {
          dir = new File(home + File.separator + ".typewriter");
          dir.mkdir();
          if (dir.canWrite()) {
            filename = home + File.separator + ".typewriter" + File.separator + ".typewriter";
          }
        }
      }
    }
    load();
    Runtime.getRuntime().addShutdownHook(new Thread(new SaveSettingsShutdownHook()));
  }

  private String get(String propertyKey, String defaultValue)
  {
    if (this.properties.containsKey(propertyKey))
      return this.properties.getProperty(propertyKey);
    if (defaultValue != null)
      this.properties.put(propertyKey, defaultValue);
    return defaultValue;
  }

  private String get(String propertyKey) {
    return this.properties.getProperty(propertyKey);
  }

  private boolean getAsBoolean(String key, boolean defaultValue) {
    if (this.properties.containsKey(key)) {
      String value = this.properties.getProperty(key);
      return Boolean.valueOf(value).booleanValue();
    }
    this.properties.put(key, "" + defaultValue);
    return defaultValue;
  }

  private int getAsInt(String key, int defaultValue)
  {
    if (this.properties.containsKey(key)) {
      String value = this.properties.getProperty(key);
      return Integer.valueOf(value).intValue();
    }
    this.properties.put(key, "" + defaultValue);
    return defaultValue;
  }

  private void set(String propertyKey, String propertyValue)
  {
    if (propertyValue == null)
      this.properties.remove(propertyKey);
    else
      this.properties.setProperty(propertyKey, propertyValue);
  }

  public void load()
  {
    try {
      InputStream stream = new FileInputStream(filename);

      this.properties.load(stream);
    }
    catch (FileNotFoundException e) {
      save();
    } catch (IOException e) {
      e.printStackTrace();
    }
  }

  public void save()
  {
    try
    {
      this.properties.store(new FileOutputStream(filename), null);
    }
    catch (IOException e) {
      System.err.println("Error saving settings to " + filename + ": " + e);
    }
  }

  private class SaveSettingsShutdownHook
    implements Runnable
  {
    public void run()
    {
      Settings.this.save();
    }

    private SaveSettingsShutdownHook()
    {
    }
  }
}
