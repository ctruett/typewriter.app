package typewriter;

import javax.swing.JFrame;
import javax.swing.UIManager;

public class Executable
{
  public static void main(String[] args)
  {
    new Executable();
  }

  public Executable() {
    try {
      UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
    }
    catch (Exception e1) {
      e1.printStackTrace();
    }

    App app = new App();
    JFrame frame = app.getFrame();
    frame.setVisible(true);
  }
}