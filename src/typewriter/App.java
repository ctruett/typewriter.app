package typewriter;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Frame;
import java.awt.GraphicsDevice;
import java.awt.GraphicsEnvironment;
import java.awt.Rectangle;
import java.awt.Toolkit;
import java.awt.Window;
import java.awt.event.ActionEvent;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.print.PrinterException;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.net.URL;
import java.util.Scanner;
import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.ImageIcon;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;
import javax.swing.event.CaretEvent;
import javax.swing.event.CaretListener;
import javax.swing.filechooser.FileFilter;
import net.roydesign.app.Application;
import net.roydesign.app.QuitJMenuItem;
import typewriter.dialog.Dialogs;

public class App extends Application
{
  public final ColorScheme standard() {
    Settings settings = new Settings();
    ColorScheme theme = settings.getColorScheme();
    return theme;
  }

  private JPanel mainPanel;
  private JFrame frame = null;
  private Window window;
  private TypeArea typeArea = null;
  private Settings settings;
  private File currentSavingFile = null;
  private boolean isModifiedAfterSave = false;
  private boolean switched = false;

  private FrameLayout fullscreenLayout = null;
  private FrameLayout windowLayout = null;

  private GraphicsDevice gs = null;
  private static int screenHeight;
  private static int screenWidth;
  private JFileChooser chooser = new JFileChooser();
  private FileFilter txtFilter;
  private boolean onMacOS = false;
  private Rectangle reserveBounds;

  private void setFrameIcon()
  {
    URL url = getClass().getResource("/icon.png");

    if (url == null) {
      return;
    }
    ImageIcon image = new ImageIcon(url);
    this.frame.setIconImage(image.getImage());
    JOptionPane.getRootFrame().setIconImage(image.getImage());
  }

  public App()
  {
    Toolkit kit = Toolkit.getDefaultToolkit();
    Dimension screenSize = kit.getScreenSize();
    screenHeight = screenSize.height;
    screenWidth = screenSize.width;

    GraphicsEnvironment ge = GraphicsEnvironment.getLocalGraphicsEnvironment();
    this.gs = ge.getDefaultScreenDevice();

    this.settings = new Settings();


		// Set up a file filter for .txt and add it to drop down!
		// Removed so I can see folders, too :D
		
    /* this.txtFilter = new FileFilter() { */
    /*   public boolean accept(File f) { */
    /*     return (f != null) && (f.getName().endsWith(".txt")); */
    /*   } */

    /*   public String getDescription() { */
    /*     return "Text files (*.txt)"; */
    /*   } */
    /* }; */
    /* this.chooser.addChoosableFileFilter(this.txtFilter); */
    /* this.chooser.setFileFilter(this.txtFilter); */

    this.frame = new JFrame();

    init();
  }

  private boolean setFullScreen(boolean fs)
  {
    boolean result = fs != this.settings.getFullScreen();

    if (!this.gs.isFullScreenSupported()) {
      this.settings.setFullScreen(false);

      Dialogs.showInfoDialog("Full screen is not supported on your system", this.settings.getColorScheme());

      return false;
    }

    if (fs) {
      Rectangle r = this.frame.getBounds();
      this.frame.remove(this.mainPanel);

      this.mainPanel.setLayout(this.fullscreenLayout);
      this.frame.pack();
      this.window.add(this.mainPanel);
      this.gs.setFullScreenWindow(this.window);
    } else {
      this.window.remove(this.mainPanel);
      this.gs.setFullScreenWindow(null);
      this.frame.add(this.mainPanel, "Center");
      this.mainPanel.setLayout(this.windowLayout);
      this.window.setVisible(false);
      Rectangle r = this.settings.getPosition();
      this.frame.setBounds(r);

      this.window.pack();
    }

    this.typeArea.requestFocus();

    this.settings.setFullScreen(fs);

    return result;
  }

  public JFrame getFrame()
  {
    return this.frame;
  }

  private void toggleFullScreen() {
    setFullScreen(!this.settings.getFullScreen());
  }

  /* private void toggleColorScheme() { */
  /*   ColorScheme currentColorScheme = this.settings.getColorScheme().equals(greenblack) ? blackwhite : greenblack; */

  /*   setColorScheme(currentColorScheme); */
  /* } */

  private void setColorScheme(ColorScheme schem) {
    final ColorScheme scheme = schem;
    SwingUtilities.invokeLater(new Runnable() {
      public void run() {
        App.this.mainPanel.setBackground(scheme.getBackground());
        App.this.frame.setBackground(scheme.getBackground());
        App.this.frame.getContentPane().setBackground(scheme.getBackground());
        App.this.window.setBackground(scheme.getBackground());
        App.this.frame.setBackground(scheme.getBackground());
        App.this.typeArea.setColorScheme(scheme);

        App.this.settings.setColorScheme(scheme);
      }
    });
  }

  private void initDialogShow(boolean pre, boolean strong)
  {
    if (pre)
    {
      this.switched = this.settings.getFullScreen();
      if (this.switched)
      {
        Dialogs.setInFrame(false);

        this.reserveBounds = this.settings.getPosition();

        this.frame.setVisible(false);
        this.window.remove(this.mainPanel);
        this.gs.setFullScreenWindow(null);
        this.frame.add(this.mainPanel, "Center");
        this.mainPanel.setLayout(this.windowLayout);

        this.mainPanel.setLayout(this.fullscreenLayout);
        this.frame.setExtendedState(6);

        this.window.setVisible(false);
        this.frame.setVisible(true);
      }
    } else if (this.switched)
    {
      setFullScreen(true);

      this.frame.setExtendedState(0);
      this.settings.setPosition(this.reserveBounds);

      Dialogs.setInFrame(true);

      SwingUtilities.invokeLater(new Runnable() {
        public void run() {
          App.this.mainPanel.updateUI();
        }
      });
    }
  }

  private void showSaveDialog()
  {
    initDialogShow(true, true);

    if (this.chooser.showSaveDialog(this.switched ? this.window : this.frame) == 0) {
      File file = this.chooser.getSelectedFile();
      try {
        saveToTxtFile(file);
        this.currentSavingFile = file;
      }
      catch (IOException e1) {
        e1.printStackTrace();
      }

    }

    initDialogShow(false, true);
  }

  private void loadFromTxtFile(File file) throws FileNotFoundException {
    Scanner scanner = new Scanner(file);
    this.typeArea.clear();
    while (scanner.hasNextLine()) {
      this.typeArea.appendLine(scanner.nextLine());
    }
    scanner.close();

    this.currentSavingFile = file;
    this.isModifiedAfterSave = false;
  }

  private void saveToTxtFile(File file) throws IOException {
    if (file == null) {
      return;
    }
    String name = file.getName();

    if ((this.chooser.getFileFilter().equals(this.txtFilter)) && (!name.contains("."))) {
      name = name + ".txt";
      file = new File(file.getParentFile(), name);
    }

    FileWriter output = new FileWriter(file);
    output.write(this.typeArea.getText());
    output.close();
    this.isModifiedAfterSave = false;
  }

  private void init()
  {
    this.frame.setDefaultCloseOperation(0);

    this.window = new Window(this.frame);

    Dialogs.init(this.frame, this.window, screenWidth, screenHeight);

    this.frame.addWindowListener(new WindowAdapter()
    {
      public void windowClosing(WindowEvent e) {
        App.this.close();
      }
    });
    setFrameIcon();
    this.frame.setTitle("Typewriter");

    this.mainPanel = new JPanel();
    this.typeArea = new TypeArea();
    this.mainPanel.add(this.typeArea);
    this.frame.getContentPane().setLayout(new BorderLayout());
    this.frame.getContentPane().add(this.mainPanel, "Center");

    setColorScheme(this.settings.getColorScheme());

    this.windowLayout = new FrameLayout(this.mainPanel, this.typeArea, 20, 20, 20, 20);

		int sideWidth = (screenWidth - this.settings.getCenterWidth()) / 2;
    this.fullscreenLayout = new FrameLayout(this.mainPanel, this.typeArea, sideWidth, 200, sideWidth, 200);

    this.frame.addFocusListener(new FocusAdapter() {
      public void focusGained(FocusEvent e) {
        App.this.typeArea.requestFocus();
      }
    });
    this.typeArea.addCaretListener(new CaretListener() {
      public void caretUpdate(CaretEvent e) {
        App.this.isModifiedAfterSave = true;
      }
    });
    Action action = new AbstractAction("close") {
      public void actionPerformed(ActionEvent e) {
        App.this.close();
      }
    };
    JMenuBar menubar = new JMenuBar();
    JMenu menu = new JMenu("System");
    QuitJMenuItem quit = getQuitJMenuItem();
    menu.add(quit);
    quit.setAction(action);
    menubar.add(menu);

    menu.add(quit);

    this.typeArea.addKeyListener(new KeyAdapter() {
      public void keyPressed(KeyEvent e) {
        if ((e.getKeyCode() == 70) && ((e.isMetaDown()) || (e.isControlDown()))) {
          App.this.toggleFullScreen();
        /* } else if ((e.getKeyCode() == 67) && ((e.isMetaDown()) || (e.isControlDown()))) { */
        /*   App.this.toggleColorScheme(); */
        } else if ((e.getKeyCode() == 80) && ((e.isMetaDown()) || (e.isControlDown())))
        {
          JavaVersion ver = JavaVersion.getCurrent();

          if (ver.compareTo(new JavaVersion("1.6")) < 0) {
            App.this.initDialogShow(true, false);
            Dialogs.showInfoDialog("<html>Printing is not supported under your java version<br>You need at least java 1.6 (you have java " + ver.getMajor() + ")<br>" + "Consider updating \n(download section on java.sun.com)", App.this.settings.getColorScheme());

            App.this.initDialogShow(false, false);
          } else {
            App.this.initDialogShow(true, true);
            try
            {
              App.this.typeArea.doPrint(App.this.frame);
            }
            catch (PrinterException e1) {
              App.this.initDialogShow(true, false);
              Dialogs.showInfoDialog("Printing error has occured", App.this.settings.getColorScheme());
              App.this.initDialogShow(false, false);
            }
            App.this.initDialogShow(false, true);
          }
        }
        else if ((e.getKeyCode() == 79) && ((e.isMetaDown()) || (e.isControlDown()))) {
          int result = 0;
          if (App.this.isModifiedAfterSave) {
            App.this.initDialogShow(true, false);
            result = Dialogs.showYesNoDialog("Changes not saved. Proceed opening file?", App.this.settings.getColorScheme());

            App.this.initDialogShow(false, false);
          }
          if (result == 0) {
            App.this.initDialogShow(true, true);
            if (App.this.chooser.showOpenDialog(App.this.switched ? App.this.window : App.this.frame) == 0) {
              File file = App.this.chooser.getSelectedFile();
              if (file.exists()) {
                try {
                  App.this.loadFromTxtFile(file);
                } catch (FileNotFoundException e1) {
                  e1.printStackTrace();
                }
              }
            }
            App.this.initDialogShow(false, true);
          }
        } else if ((e.getKeyCode() == 87) && ((e.isMetaDown()) || (e.isControlDown()))) {
          App.this.initDialogShow(true, true);
          App.this.showSaveDialog();
          App.this.initDialogShow(false, true);
        } else if ((e.getKeyCode() == 83) && ((e.isMetaDown()) || (e.isControlDown()))) {
          if (App.this.currentSavingFile == null) {
            App.this.initDialogShow(true, true);
            App.this.showSaveDialog();
            App.this.initDialogShow(false, true);
          } else {
            try {
              App.this.saveToTxtFile(App.this.currentSavingFile);
            } catch (IOException e1) {
              e1.printStackTrace();
            }
          }
        } else if ((e.getKeyCode() == 78) && ((e.isMetaDown()) || (e.isControlDown())))
        {
          int result = 0;

          if (App.this.isModifiedAfterSave) {
            App.this.initDialogShow(true, false);
            result = Dialogs.showYesNoDialog("Changes not saved. Proceed file creation?", App.this.settings.getColorScheme());

            App.this.initDialogShow(false, false);
          }
          if (result == 0) {
            App.this.typeArea.clear();
            App.this.currentSavingFile = null;
            App.this.isModifiedAfterSave = false;
          }
        } else if ((e.getKeyCode() == 75) && ((e.isMetaDown()) || (e.isControlDown()))) {
          App.this.initDialogShow(true, false);
          Dialogs.showHelpDialog(App.this.settings.getColorScheme());
          App.this.initDialogShow(false, false);
        } else if ((e.getKeyCode() == 81) && ((e.isMetaDown()) || (e.isControlDown()))) {
          App.this.close();
        }
      }
    });
    final boolean fs = this.settings.getFullScreen();
    Rectangle r = this.settings.getPosition();
    this.typeArea.setTheSize(r.width, r.height);
    this.frame.setBounds(this.settings.getPosition());

    SwingUtilities.invokeLater(new Runnable() {
      public void run() {
        App.this.setFullScreen(fs);
      }
    });
    if (this.settings.getShowKeys()) {
      Runnable runnable = new Runnable() {
        public void run() {
          App.this.initDialogShow(true, false);
          Dialogs.showHelpDialog(App.this.settings.getColorScheme());
          App.this.initDialogShow(false, false);
        }
      };
      this.settings.setShowKeys(false);

      SwingUtilities.invokeLater(runnable);
    }

    this.frame.addComponentListener(new ComponentAdapter() {
      public void componentResized(ComponentEvent e) {
        if (!App.this.settings.getFullScreen())
          App.this.settings.setPosition(App.this.frame.getBounds());
      }

      public void componentMoved(ComponentEvent e)
      {
        if (!App.this.settings.getFullScreen())
          App.this.settings.setPosition(App.this.frame.getBounds());
      }
    });
  }

  public void close()
  {
    int result = 0;
    if (this.isModifiedAfterSave) {
      initDialogShow(true, false);
      result = Dialogs.showYesNoDialog("Changes not saved. Exit anyway?", this.settings.getColorScheme());

      initDialogShow(false, false);
    }

    if (result == 0)
      System.exit(0);
  }
}
