package typewriter;

import java.awt.print.PrinterException;

public abstract interface PrinterInterface
{
  public abstract void print(String paramString)
    throws PrinterException;
}