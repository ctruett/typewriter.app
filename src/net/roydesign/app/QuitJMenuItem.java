package net.roydesign.app;

import java.awt.Toolkit;
import java.awt.event.ActionListener;
import javax.swing.AbstractButton;
import javax.swing.JMenuItem;
import javax.swing.KeyStroke;

public class QuitJMenuItem extends JMenuItem
{
  QuitJMenuItem(Application application)
  {
    super("Quit");
    setAccelerator(KeyStroke.getKeyStroke(81, Toolkit.getDefaultToolkit().getMenuShortcutKeyMask()));

    String appName = application.getName();
  }

  public void addActionListener(ActionListener l)
  {
    super.addActionListener(l);
  }

  public void removeActionListener(ActionListener l)
  {
    super.removeActionListener(l);
  }

}