package net.roydesign.app;

import java.awt.event.ActionListener;
import javax.swing.AbstractButton;
import javax.swing.JMenuItem;

public class AboutJMenuItem extends JMenuItem
{
  AboutJMenuItem(Application application)
  {
    super("About");
    String appName = application.getName();
    if (appName != null)
      setText("About " + appName);
  }

  public void addActionListener(ActionListener l)
  {
    super.addActionListener(l);
  }

  public void removeActionListener(ActionListener l)
  {
    super.removeActionListener(l);
  }

}