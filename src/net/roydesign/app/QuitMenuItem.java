package net.roydesign.app;

import java.awt.MenuItem;
import java.awt.MenuShortcut;
import java.awt.event.ActionListener;

public class QuitMenuItem extends MenuItem
{
  QuitMenuItem(Application application)
  {
    super("Quit", new MenuShortcut(81));
    String appName = application.getName();
  }

  public void addActionListener(ActionListener l)
  {
    super.addActionListener(l);
  }

  public void removeActionListener(ActionListener l)
  {
    super.removeActionListener(l);
  }

}