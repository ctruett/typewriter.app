package net.roydesign.app;

import java.awt.MenuItem;
import java.awt.event.ActionListener;

public class PreferencesMenuItem extends MenuItem
{
  PreferencesMenuItem()
  {
    super("Preferences");
  }

  public void addActionListener(ActionListener l)
  {
    super.addActionListener(l);
  }

  public void removeActionListener(ActionListener l)
  {
    super.removeActionListener(l);
  }

  public void setEnabled(boolean enabled)
  {
    super.setEnabled(enabled);
  }

}