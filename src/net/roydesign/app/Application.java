package net.roydesign.app;

import java.awt.MenuBar;
import java.awt.event.ActionListener;
import javax.swing.JMenuBar;

public class Application
{
  private static Application instance;
  private String name;

  protected Application()
  {
    if (instance != null)
      throw new IllegalStateException();
    instance = this;
  }

  public static synchronized Application getInstance()
  {
    if (instance == null)
      new Application();
    return instance;
  }

  public void setName(String name)
  {
    this.name = name;
  }

  public String getName()
  {
    return this.name;
  }

  public AboutJMenuItem getAboutJMenuItem()
  {
    return new AboutJMenuItem(this);
  }

  public AboutMenuItem getAboutMenuItem()
  {
    return new AboutMenuItem(this);
  }

  public PreferencesJMenuItem getPreferencesJMenuItem()
  {
    return new PreferencesJMenuItem();
  }

  public PreferencesMenuItem getPreferencesMenuItem()
  {
    return new PreferencesMenuItem();
  }

  public QuitJMenuItem getQuitJMenuItem()
  {
    return new QuitJMenuItem(this);
  }

  public QuitMenuItem getQuitMenuItem()
  {
    return new QuitMenuItem(this);
  }

}