package net.roydesign.app;

import java.awt.MenuItem;
import java.awt.event.ActionListener;

public class AboutMenuItem extends MenuItem
{
  AboutMenuItem(Application application)
  {
    super("About");
    String appName = application.getName();
    if (appName != null)
      setLabel("About " + appName);
  }

  public void addActionListener(ActionListener l)
  {
    super.addActionListener(l);
  }

  public void removeActionListener(ActionListener l)
  {
    super.removeActionListener(l);
  }

}