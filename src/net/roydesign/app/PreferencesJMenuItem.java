package net.roydesign.app;

import java.awt.event.ActionListener;
import javax.swing.AbstractButton;
import javax.swing.JMenuItem;

public class PreferencesJMenuItem extends JMenuItem
{
  PreferencesJMenuItem()
  {
    super("Preferences");
  }

  public void addActionListener(ActionListener l)
  {
    super.addActionListener(l);
  }

  public void removeActionListener(ActionListener l)
  {
    super.removeActionListener(l);
  }

  public void setEnabled(boolean enabled)
  {
    super.setEnabled(enabled);
  }

}