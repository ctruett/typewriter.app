package net.roydesign.io;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;

public class ApplicationFile
{
  private static final String osName = System.getProperty("os.name");
  File executable;

  public ApplicationFile(String path)
  {
    this.executable = new File(path);
  }

  public ApplicationFile(String parent, String child)
  {
    this.executable = new File(parent, child);
  }

  public ApplicationFile(File parent, String child)
  {
    this.executable = new File(parent, child);
  }

  public ApplicationFile(File executable)
  {
    this(executable.getPath());
  }

  public boolean open()
    throws IOException
  {
    if (osName.startsWith("Windows"))
    {
      try
      {
        Process p = Runtime.getRuntime().exec(new String[] { "cmd", "/c", "start", "\"\"", this.executable.getAbsolutePath() });

        if (p.waitFor() != 0)
          return false;
      }
      catch (InterruptedException e)
      {
        return false;
      }

    }
    else
    {
      try
      {
        Process p = Runtime.getRuntime().exec(new String[] { this.executable.getAbsolutePath() });
        if (p.waitFor() != 0)
          return false;
      }
      catch (InterruptedException e)
      {
        return false;
      }

    }

    return true;
  }

  public Process open(String[] args)
    throws IOException
  {
    String[] nargs = new String[args.length + 1];
    nargs[0] = this.executable.getAbsolutePath();
    System.arraycopy(args, 0, nargs, 1, args.length);
    return Runtime.getRuntime().exec(nargs);
  }

  public boolean openDocument(DocumentFile documentFile)
    throws IOException
  {
    return openDocument(documentFile.file);
  }

  public boolean openDocument(File file)
    throws IOException
  {
    return openDocuments(new File[] { file });
  }

  public boolean openDocuments(DocumentFile[] documentFiles)
    throws IOException
  {
    File[] files = new File[documentFiles.length];
    for (int i = 0; i < files.length; i++)
      files[i] = documentFiles[i].file;
    return openDocuments(files);
  }

  public boolean openDocuments(File[] files)
    throws IOException
  {
    if (osName.startsWith("Windows"))
    {
      try
      {
        String[] strs = new String[5 + files.length];
        strs[0] = "cmd";
        strs[1] = "/c";
        strs[2] = "start";
        strs[3] = "\"\"";
        strs[4] = this.executable.getAbsolutePath();
        for (int i = 0; i < files.length; i++)
          strs[(5 + i)] = files[i].getAbsolutePath();
        Process p = Runtime.getRuntime().exec(strs);
        if (p.waitFor() != 0)
          return false;
      }
      catch (InterruptedException e)
      {
        return false;
      }

    }
    else
    {
      try
      {
        String[] strs = new String[1 + files.length];
        strs[0] = this.executable.getAbsolutePath();
        for (int i = 0; i < files.length; i++)
          strs[(1 + i)] = files[i].getAbsolutePath();
        Process p = Runtime.getRuntime().exec(strs);
        if (p.waitFor() != 0)
          return false;
      }
      catch (InterruptedException e)
      {
        return false;
      }

    }

    return true;
  }

  public String getPath()
  {
    return this.executable.getPath();
  }

  public String getAbsolutePath()
  {
    return this.executable.getAbsolutePath();
  }

  public String getCanonicalPath()
    throws IOException
  {
    return this.executable.getCanonicalPath();
  }

  public String getExecutableName()
  {
    return this.executable.getName();
  }

  public String getDisplayedName()
    throws IOException
  {
    if (!osName.startsWith("Windows"));
    return getExecutableName();
  }

}