package net.roydesign.io;

import java.io.File;
import java.io.IOException;

public class DocumentFile
{
  private static final String osName = System.getProperty("os.name");
  File file;

  public DocumentFile(String path)
  {
    this.file = new File(path);
  }

  public DocumentFile(String parent, String child)
  {
    this.file = new File(parent, child);
  }

  public DocumentFile(File parent, String child)
  {
    this.file = new File(parent, child);
  }

  public DocumentFile(File file)
  {
    this(file.getPath());
  }

  public boolean open()
    throws IOException
  {
    if (osName.startsWith("Windows"))
    {
      try
      {
        Process p = Runtime.getRuntime().exec(new String[] { "cmd", "/c", "start", "\"\"", this.file.getAbsolutePath() });

        if (p.waitFor() != 0)
          return false;
      }
      catch (InterruptedException e)
      {
        return false;
      }

    }
    else
    {
      return false;
    }

    return true;
  }

  public boolean openWith(ApplicationFile application)
    throws IOException
  {
    return application.openDocument(this);
  }

  public boolean openWith(File application)
    throws IOException
  {
    return openWith(new ApplicationFile(application));
  }

  public void setExtension(String extension)
    throws IOException
  {
    StringBuffer b = new StringBuffer();
    b.append(getTitle());
    if ((extension != null) && (extension.length() > 0))
    {
      b.append('.');
      b.append(extension);
    }
    File f = new File(this.file.getParent(), b.toString());
    if (!this.file.renameTo(f))
      throw new IOException("failed to rename file");
    this.file = f;
  }

  public String getExtension()
    throws IOException
  {
    String n = this.file.getName();
    int pos = n.lastIndexOf('.');
    if ((pos != -1) && (pos + 1 != n.length()))
      return n.substring(pos + 1);
    return "";
  }

  public void setTitle(String title)
    throws IOException
  {
    if ((title == null) || (title.length() == 0))
      throw new IllegalArgumentException("title can't be null or zero length");
    StringBuffer b = new StringBuffer();
    b.append(title);
    String ext = getExtension();
    if ((ext != null) && (ext.length() > 0))
    {
      b.append('.');
      b.append(ext);
    }
    File f = new File(this.file.getParent(), b.toString());
    if (!this.file.renameTo(f))
      throw new IOException("failed to rename file");
    this.file = f;
  }

  public String getTitle()
    throws IOException
  {
    String n = this.file.getName();
    int pos = n.lastIndexOf('.');
    if ((pos != -1) && (pos != 0) && (pos + 1 != n.length()))
      return n.substring(0, pos);
    return n;
  }

  public void setTitleAndExtension(String title, String extension)
    throws IOException
  {
    if ((title == null) || (title.length() == 0))
      throw new IllegalArgumentException("title can't be null or zero length");
    StringBuffer b = new StringBuffer();
    b.append(title);
    if ((extension != null) && (extension.length() > 0))
    {
      b.append('.');
      b.append(extension);
    }
    File f = new File(this.file.getParent(), b.toString());
    if (!this.file.renameTo(f))
      throw new IOException("failed to rename file");
    this.file = f;
  }

  public File getFile()
  {
    return this.file;
  }

  public String getPath()
  {
    return this.file.getPath();
  }

  public String getAbsolutePath()
  {
    return this.file.getAbsolutePath();
  }

  public String getCanonicalPath()
    throws IOException
  {
    return this.file.getCanonicalPath();
  }
}