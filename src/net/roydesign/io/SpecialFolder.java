package net.roydesign.io;

import java.io.File;
import java.io.FileNotFoundException;

public class SpecialFolder
{
  private static final String osName = System.getProperty("os.name");

  public static File getHomeFolder()
  {
    return new File(System.getProperty("user.home"));
  }

  public static File getPreferencesFolder()
    throws FileNotFoundException
  {

    if (osName.startsWith("Windows"))
    {
      return new File(System.getProperty("user.home"), "Application Data");
    }

    return new File(System.getProperty("user.home"));
  }

  public static File getTemporaryItemsFolder()
    throws FileNotFoundException
  {

    if (osName.startsWith("Windows"))
    {
      return new File("c:\temp\"");
    }

    throw new FileNotFoundException();
  }

  public static File getDesktopFolder()
    throws FileNotFoundException
  {

    if (osName.startsWith("Windows"))
    {
      return new File(System.getProperty("user.home"), "Desktop");
    }
    throw new FileNotFoundException();
  }

}