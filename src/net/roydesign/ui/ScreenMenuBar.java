package net.roydesign.ui;

import java.awt.Frame;
import java.awt.Menu;
import java.awt.MenuBar;
import java.awt.MenuComponent;
import java.awt.MenuItem;

public class ScreenMenuBar extends MenuBar
{
  public void addNotify()
  {
    Frame f = (Frame)getParent();

    int n = getMenuCount();
    for (int i = n - 1; i >= 0; i--)
    {
      Menu m = getMenu(i);
      if (((m instanceof ScreenMenu)) && (!((ScreenMenu)m).isUsedBy(f)))
      {
	  remove(i);
      }
    }
    super.addNotify();
  }
}