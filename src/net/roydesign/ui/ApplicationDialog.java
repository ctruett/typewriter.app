package net.roydesign.ui;

import java.awt.Dialog;
import java.awt.FileDialog;
import java.awt.Frame;
import java.io.File;
import java.io.FilenameFilter;
import java.io.IOException;
import java.util.Hashtable;
import java.util.Properties;
import net.roydesign.io.ApplicationFile;

public class ApplicationDialog extends FileDialog
{
  private boolean modeCheckingEnabled = false;

  public ApplicationDialog(Frame parent)
  {
    this(parent, "");
  }

  public ApplicationDialog(Frame parent, String title)
  {
    super(parent, title, 0);
    setFilenameFilter(new ApplicationFilter(null));
    this.modeCheckingEnabled = true;
  }

  public ApplicationFile getApplicationFile()
  {
    String f = getFile();
    return f != null ? new ApplicationFile(getDirectory(), f) : null;
  }

  public void setMode(int mode)
  {
    if (this.modeCheckingEnabled)
      throw new Error("can't set mode");
    super.setMode(mode);
  }

  public void show()
  {
    String prop = null;
    Properties props = System.getProperties();
    Object oldValue = null;
    if (prop != null)
    {
      oldValue = props.get(prop);
      props.put(prop, "true");
    }

    super.show();

    if (prop != null)
    {
      if (oldValue == null)
        props.remove(prop);
      else
        props.put(prop, oldValue);
    }
  }

  private class ApplicationFilter
    implements FilenameFilter
  {
    private ApplicationFilter()
    {
    }

    public boolean accept(File directory, String name)
    {
      return true;
    }

    ApplicationFilter(ApplicationDialog x1)
    {
      this();
    }
  }
}