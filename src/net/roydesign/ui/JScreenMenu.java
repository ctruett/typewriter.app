package net.roydesign.ui;

import java.awt.Component;
import java.awt.Container;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.Vector;
import javax.swing.Action;
import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.JSeparator;

public class JScreenMenu extends JMenu
  implements PropertyChangeListener
{
  private static ComponentAdapter initialStateSetterMRJ3 = new ComponentAdapter()
  {
    public void componentResized(ComponentEvent e)
    {
      Component comp = e.getComponent();
      comp.removeComponentListener(this);
      if (!comp.isEnabled())
      {
        comp.setEnabled(true);
        comp.setEnabled(false);
      }
    }
  };
  private Vector userFrames;

  public JScreenMenu()
  {
    super("");
    init();
  }

  public JScreenMenu(String text)
  {
    super(text);
    init();
  }

  private void init()
  {
  }

  public JMenuItem add(JMenuItem menuItem)
  {
    menuItem.addPropertyChangeListener(this);
    return super.add(menuItem);
  }

  public Component add(Component comp)
  {
    comp.addPropertyChangeListener(this);
    return super.add(comp);
  }

  public Component add(Component comp, int index)
  {
    comp.addPropertyChangeListener(this);
    return super.add(comp, index);
  }

  public void remove(JMenuItem menuItem)
  {
    menuItem.removePropertyChangeListener(this);
    super.remove(menuItem);
  }

  public void remove(int index)
  {
    getItem(index).removePropertyChangeListener(this);
    super.remove(index);
  }

  public void remove(Component comp)
  {
    comp.removePropertyChangeListener(this);
    super.remove(comp);
  }

  public void removeAll()
  {
    int n = getMenuComponentCount();
    for (int i = 0; i < n; i++)
      getMenuComponent(i).removePropertyChangeListener(this);
    super.removeAll();
  }

  public void addNotify()
  {
    JFrame f = getParentFrame();

    boolean enabled = false;
    boolean hasSeparator = true;
    int n = getMenuComponentCount();
    for (int i = n - 1; i >= 0; i--)
    {
      Component comp = getMenuComponent(i);
      if ((comp instanceof JSeparator))
      {
        if (hasSeparator)
        {
          comp.setVisible(false);
          continue;
        }
        hasSeparator = true;
      }
      else if ((comp instanceof JScreenMenuItem))
      {
        JScreenMenuItem mi = (JScreenMenuItem)comp;
        Action a = mi.getAction();
        if (((a != null) && ((a instanceof AbstractScreenAction)) && (!((AbstractScreenAction)a).isUsedBy(f))) || (!mi.isUsedBy(f)))
        {
            mi.setVisible(false);
        }
        else
        {
          hasSeparator = false;
        }
      }
      else if ((comp instanceof JScreenMenu))
      {
        JScreenMenu m = (JScreenMenu)comp;
        if (!m.isUsedBy(f))
        {
        }
        else {
          hasSeparator = false;
        }
        m.addNotify();
      }
      else
      {
        hasSeparator = false;
      }
      if ((comp.isVisible()) && (comp.isEnabled()) && (!(comp instanceof JSeparator))) {
        enabled = true;
      }
    }

    if (!enabled) {
      setEnabled(false);
    }
    super.addNotify();
  }

  public synchronized void addUserFrame(Class frameClass)
  {
    if (this.userFrames == null)
      this.userFrames = new Vector();
    this.userFrames.addElement(frameClass);
  }

  public synchronized void removeUserFrame(Class frameClass)
  {
    if (this.userFrames == null)
      return;
    this.userFrames.removeElement(frameClass);
    if (this.userFrames.size() == 0)
      this.userFrames = null;
  }

  public boolean isUsedBy(JFrame frame)
  {
    return (this.userFrames == null) || (this.userFrames.contains(frame.getClass()));
  }

  protected JFrame getParentFrame()
  {
    Component comp = getParent();
    while ((comp != null) && (!(comp instanceof JFrame)))
      comp = ((Container)comp).getParent();
    return (JFrame)comp;
  }

  public void propertyChange(PropertyChangeEvent e)
  {
    if (e.getPropertyName().equals("enabled"))
    {
      if (((Boolean)e.getNewValue()).booleanValue() == true)
      {
        setEnabled(true);
      }
      else
      {
        int n = getMenuComponentCount();
        for (int i = 0; i < n; i++)
        {
          Component comp = getMenuComponent(i);
          if ((comp.isVisible()) && (comp.isEnabled()) && (!(comp instanceof JSeparator)))
            return;
        }
        setEnabled(false);
      }
    }
  }
}