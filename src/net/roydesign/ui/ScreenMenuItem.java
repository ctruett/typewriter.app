package net.roydesign.ui;

import java.awt.Frame;
import java.awt.MenuItem;
import java.awt.MenuShortcut;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.lang.reflect.Method;
import java.util.Vector;
import javax.swing.Action;

public class ScreenMenuItem extends MenuItem
{
  private Action action;
  private PropertyChangeListener actionPropertyChangeListener = new PropertyChangeListener()
  {
    public void propertyChange(PropertyChangeEvent e)
    {
      String prop = e.getPropertyName();
      if (prop.equals("action"))
        ScreenMenuItem.this.configurePropertiesFromAction((Action)e.getNewValue());
      else if (prop.equals("Name"))
        ScreenMenuItem.this.setLabel((String)e.getNewValue());
      else if (prop.equals("enabled"))
        ScreenMenuItem.this.setEnabled(((Boolean)e.getNewValue()).booleanValue());
      else if (prop.equals("ActionCommandKey"))
        ScreenMenuItem.this.setActionCommand((String)e.getNewValue());
    }
  };

  private PropertyChangeSupport propertiesHandler = new PropertyChangeSupport(this);
  private Vector userFrames;

  public ScreenMenuItem()
  {
  }

  public ScreenMenuItem(String text)
  {
    super(text);
  }

  public ScreenMenuItem(String text, MenuShortcut shortcut)
  {
    super(text, shortcut);
  }

  public ScreenMenuItem(Action action)
  {
    setAction(action);
  }

  public synchronized void setLabel(String label)
  {
    String oldLabel = getLabel();
    super.setLabel(label);
    if (!label.equals(oldLabel))
      this.propertiesHandler.firePropertyChange("label", oldLabel, label);
  }

  public synchronized void setEnabled(boolean enabled)
  {
    boolean oldEnabled = isEnabled();
    super.setEnabled(enabled);
    if (enabled != oldEnabled)
    {
      this.propertiesHandler.firePropertyChange("enabled", new Boolean(oldEnabled), new Boolean(enabled));
    }
  }

  public void setShortcut(MenuShortcut shortcut)
  {
    MenuShortcut oldShortcut = getShortcut();
    super.setShortcut(shortcut);
    if (shortcut != oldShortcut)
    {
      this.propertiesHandler.firePropertyChange("shortcut", oldShortcut, shortcut);
    }
  }

  public Action getAction()
  {
    return this.action;
  }

  public void setAction(Action action)
  {
    Action oldAction = this.action;
    if ((oldAction == null) || (!oldAction.equals(action)))
    {
      this.action = action;
      if (oldAction != null)
      {
        removeActionListener(oldAction);
        oldAction.removePropertyChangeListener(this.actionPropertyChangeListener);
      }
      configurePropertiesFromAction(this.action);
      if (this.action != null)
      {
        addActionListener(this.action);
        this.action.addPropertyChangeListener(this.actionPropertyChangeListener);
      }
    }
  }

  protected void configurePropertiesFromAction(Action action)
  {
    if (action != null)
    {
      setLabel((String)action.getValue("Name"));
      setEnabled(action.isEnabled());
      Object ks = action.getValue("AcceleratorKey");
      if (ks != null)
      {
        try
        {
          Method met = ks.getClass().getMethod("getModifiers");
          Object obj = met.invoke(ks);
          int mdfrs = ((Number)obj).intValue();
          if ((mdfrs & 0x4) != 0)
          {
            boolean shft = (mdfrs & 0x1) != 0;
            met = ks.getClass().getMethod("getKeyCode");
            obj = met.invoke(ks);
            int code = ((Number)obj).intValue();
            setShortcut(new MenuShortcut(code, shft));
          }

        }
        catch (Exception e)
        {
          e.printStackTrace();
        }
      }
      else
      {
        setShortcut(null);
      }
    }
    else
    {
      setLabel(null);
      setEnabled(true);
      setShortcut(null);
    }
  }

  public synchronized void addUserFrame(Class frameClass)
  {
    if (this.userFrames == null)
      this.userFrames = new Vector();
    this.userFrames.addElement(frameClass);
  }

  public synchronized void removeUserFrame(Class frameClass)
  {
    if (this.userFrames == null)
      return;
    this.userFrames.removeElement(frameClass);
    if (this.userFrames.size() == 0)
      this.userFrames = null;
  }

  public boolean isUsedBy(Frame frame)
  {
    return (this.userFrames == null) || (this.userFrames.contains(frame.getClass()));
  }

  public void addPropertyChangeListener(PropertyChangeListener l)
  {
    this.propertiesHandler.addPropertyChangeListener(l);
  }

  public void removePropertyChangeListener(PropertyChangeListener l)
  {
    this.propertiesHandler.removePropertyChangeListener(l);
  }
}