package net.roydesign.ui;

import java.awt.Component;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.Vector;
import javax.swing.AbstractButton;
import javax.swing.Action;
import javax.swing.Icon;
import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JMenuItem;
import javax.swing.KeyStroke;

public class JScreenMenuItem extends JMenuItem
{
  private Action actionBefore13;
  private PropertyChangeListener actionPropertyChangeListener = new PropertyChangeListener()
  {
    public void propertyChange(PropertyChangeEvent e)
    {
      if (e.getPropertyName().equals("action"))
        JScreenMenuItem.this.configurePropertiesFromAction((Action)e.getNewValue());
    }
  };
  private Vector userFrames;

  public JScreenMenuItem()
  {
  }

  public JScreenMenuItem(Icon icon)
  {
    super(icon);
  }

  public JScreenMenuItem(String text)
  {
    super(text);
  }

  public JScreenMenuItem(Action action)
  {
    setAction(action);
  }

  public JScreenMenuItem(String text, Icon icon)
  {
    super(text, icon);
  }

  public JScreenMenuItem(String text, int mnemonic)
  {
    super(text, mnemonic);
  }

  public Action getAction()
  {
    return super.getAction();
  }

  public void setAction(Action action)
  {
      super.setAction(action);
  }

  private void setActionBefore13(Action action)
  {
    Action oldAction = this.actionBefore13;
    if ((oldAction == null) || (!oldAction.equals(action)))
    {
      this.actionBefore13 = action;
      if (oldAction != null)
      {
        removeActionListener(oldAction);
        oldAction.removePropertyChangeListener(this.actionPropertyChangeListener);
      }
      configurePropertiesFromAction(this.actionBefore13);
      if (this.actionBefore13 != null)
      {
        addActionListener(this.actionBefore13);
        this.actionBefore13.addPropertyChangeListener(this.actionPropertyChangeListener);
      }
      firePropertyChange("action", oldAction, this.actionBefore13);
      revalidate();
      repaint();
    }
  }

  protected void configurePropertiesFromAction(Action action)
  {
      setText(action != null ? (String)action.getValue("Name") : null);
      setIcon(action != null ? (Icon)action.getValue("SmallIcon") : null);
      setAccelerator(action != null ? (KeyStroke)action.getValue("AcceleratorKey") : null);
      setEnabled(action != null ? action.isEnabled() : true);
      setToolTipText(action != null ? (String)action.getValue("ShortDescription") : null);
      if (action != null)
      {
        Integer i = (Integer)action.getValue("MnemonicKey");
        if (i != null)
          setMnemonic(i.intValue());
      }
  }

  public synchronized void addUserFrame(Class frameClass)
  {
    if (this.userFrames == null)
      this.userFrames = new Vector();
    this.userFrames.addElement(frameClass);
  }

  public synchronized void removeUserFrame(Class frameClass)
  {
    if (this.userFrames == null)
      return;
    this.userFrames.removeElement(frameClass);
    if (this.userFrames.size() == 0)
      this.userFrames = null;
  }

  public boolean isUsedBy(JFrame frame)
  {
    return (this.userFrames == null) || (this.userFrames.contains(frame.getClass()));
  }
}