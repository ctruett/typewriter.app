package net.roydesign.ui;

import java.awt.Frame;
import java.awt.Menu;
import java.awt.MenuComponent;
import java.awt.MenuContainer;
import java.awt.MenuItem;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.Vector;
import javax.swing.Action;

public class ScreenMenu extends Menu
  implements PropertyChangeListener
{
  private Vector userFrames;

  public ScreenMenu()
  {
    super("");
  }

  public ScreenMenu(String text)
  {
    super(text);
  }

  public ScreenMenu(String text, boolean tearOff)
  {
    super(text, tearOff);
  }

  public MenuItem add(MenuItem menuItem)
  {
    if ((menuItem instanceof ScreenMenuItem))
      ((ScreenMenuItem)menuItem).addPropertyChangeListener(this);
    return super.add(menuItem);
  }

  public void insert(MenuItem menuItem, int index)
  {
    if ((menuItem instanceof ScreenMenuItem))
      ((ScreenMenuItem)menuItem).addPropertyChangeListener(this);
    super.insert(menuItem, index);
  }

  public void remove(int index)
  {
    MenuItem it = getItem(index);
    if ((it instanceof ScreenMenuItem))
      ((ScreenMenuItem)it).removePropertyChangeListener(this);
    super.remove(index);
  }

  public void addNotify()
  {
    Frame f = getParentFrame();

    boolean enabled = false;
    boolean hasSeparator = true;
    int n = getItemCount();
    for (int i = n - 1; i >= 0; i--)
    {
      MenuItem it = getItem(i);
      if (it.getLabel().equals("-"))
      {
        if (hasSeparator)
        {
          remove(i);
          continue;
        }
        hasSeparator = true;
      }
      else if ((it instanceof ScreenMenuItem))
      {
        ScreenMenuItem mi = (ScreenMenuItem)it;
        Action a = mi.getAction();
        if (((a != null) && ((a instanceof AbstractScreenAction)) && (!((AbstractScreenAction)a).isUsedBy(f))) || (!mi.isUsedBy(f)))
        {
            remove(i);
        }
        else
        {
          hasSeparator = false;
        }
      }
      else if ((it instanceof ScreenMenu))
      {
        ScreenMenu m = (ScreenMenu)it;
        if (!m.isUsedBy(f))
        {
            remove(i);
        }
        else
        {
          hasSeparator = false;
        }

      }
      else
      {
        hasSeparator = false;
      }
      if ((it.getParent() != null) && (it.isEnabled()) && (!it.getLabel().equals("-"))) {
        enabled = true;
      }
    }

    if (!enabled) {
      setEnabled(false);
    }
    super.addNotify();
  }

  public synchronized void addUserFrame(Class frameClass)
  {
    if (this.userFrames == null)
      this.userFrames = new Vector();
    this.userFrames.addElement(frameClass);
  }

  public synchronized void removeUserFrame(Class frameClass)
  {
    if (this.userFrames == null)
      return;
    this.userFrames.removeElement(frameClass);
    if (this.userFrames.size() == 0)
      this.userFrames = null;
  }

  public boolean isUsedBy(Frame frame)
  {
    return (this.userFrames == null) || (this.userFrames.contains(frame.getClass()));
  }

  protected Frame getParentFrame()
  {
    MenuContainer cont = getParent();
    while ((cont != null) && (!(cont instanceof Frame)))
      cont = ((MenuComponent)cont).getParent();
    return (Frame)cont;
  }

  public void propertyChange(PropertyChangeEvent e)
  {
    if (e.getPropertyName().equals("enabled"))
    {
      if (((Boolean)e.getNewValue()).booleanValue() == true)
      {
        setEnabled(true);
      }
      else
      {
        int n = getItemCount();
        for (int i = 0; i < n; i++)
        {
          MenuItem it = getItem(i);
          if ((it.isEnabled()) && (!it.getLabel().equals("-")))
            return;
        }
        setEnabled(false);
      }
    }
  }
}