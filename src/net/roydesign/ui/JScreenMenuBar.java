package net.roydesign.ui;

import java.awt.Component;
import java.awt.Container;
import javax.swing.JFrame;
import javax.swing.JMenuBar;

public class JScreenMenuBar extends JMenuBar
{
  public void addNotify()
  {
    JFrame f = getParentFrame();

    int n = getComponentCount();
    for (int i = n - 1; i >= 0; i--)
    {
      Component m = getComponent(i);
      if (((m instanceof JScreenMenu)) && (!((JScreenMenu)m).isUsedBy(f)))
      {
          m.setVisible(false);
      }
    }
    super.addNotify();
  }

  protected JFrame getParentFrame()
  {
    Component comp = getParent();
    while ((comp != null) && (!(comp instanceof JFrame)))
      comp = ((Container)comp).getParent();
    return (JFrame)comp;
  }
}