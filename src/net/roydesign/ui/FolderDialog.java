package net.roydesign.ui;

import java.awt.Dialog;
import java.awt.FileDialog;
import java.awt.Frame;
import java.io.File;
import java.util.Hashtable;
import java.util.Properties;

public class FolderDialog extends FileDialog
{
  private boolean modeCheckingEnabled = false;

  public FolderDialog(Frame parent)
  {
    this(parent, "");
  }

  public FolderDialog(Frame parent, String title)
  {
    super(parent, title, getInitialMode());
    this.modeCheckingEnabled = true;
  }

  public String getFile()
  {
    return super.getFile() != null ? "" : null;
  }

  public String getDirectory()
  {
    String path = super.getDirectory();
    if (path == null)
      return null;
    return path;
  }

  public void setMode(int mode)
  {
    if (this.modeCheckingEnabled)
      throw new Error("can't set mode");
    super.setMode(mode);
  }

  public void show()
  {
    String prop = null;
    Properties props = System.getProperties();
    Object oldValue = null;
    if (prop != null)
    {
      oldValue = props.get(prop);
      props.put(prop, "true");
    }

    super.show();

    if (prop != null)
    {
      if (oldValue == null)
        props.remove(prop);
      else
        props.put(prop, oldValue);
    }
  }

  private static int getInitialMode()
  {
    return 1;
  }
}